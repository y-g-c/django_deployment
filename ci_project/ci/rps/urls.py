import path as path
from django.conf.urls import url
from django.urls import path

from . import views


urlpatterns = [
    path('hello-world/', views.helloWorld, name='hello-world'),
    path('index/', views.index, name='index'),
    path('rock/', views.rock, name='rock'),
    path('paper/', views.paper, name='paper'),
    path('scissors/', views.scissors, name='scissors'),
    path('lizard/', views.lizard, name='lizard'),
    path('spock/', views.spock, name='spock'),

]
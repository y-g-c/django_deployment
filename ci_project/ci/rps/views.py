import random

from django.shortcuts import render
from django.http import HttpResponse

choices = ['rock', 'paper', 'scissors', 'lizard', 'Spock']


# Create your views here.

def helloWorld(request):
    # return HttpResponse("Hello World !!")
    # return render(request, 'base.html')
    p = 'Hello world'
    return render(request, 'hello-world.html', {'p': p})


def index(request):
    return render(request, 'index.html', )


def rock(request):
    choice = 'rock'
    computer = random.choice(choices)

    if computer == 'paper':
        result = 'paper covers rock, you loose'
    elif computer == 'lizard':
        result = 'rock crushes lizard, you win'
    elif computer == 'Spock':
        result = 'Spock vaporizes rock, you loose'
    elif computer == 'scissors':
        result = 'rock crushes scissors, you win'
    elif computer == 'rock':
        result = 'tie'
    else:
        result = '????'

    return render(request, 'rock.html', {'choice': choice, 'result': result, 'computer': computer})


def paper(request):
    choice = 'Paper'
    computer = random.choice(choices)

    if computer == 'scissors':
        result = 'scissors cut paper, you loose'
    elif computer == 'rock':
        result = 'paper covers rock, you win'
    elif computer == 'lizard':
        result = 'lizard eats paper, you loose'
    elif computer == 'Spock':
        result = 'paper disproves Spock, you win'
    elif computer == 'paper':
        result = 'tie'
    else:
        result = '????'

    return render(request, 'paper.html', {'choice': choice, 'result': result, 'computer': computer})


def scissors(request):
    choice = 'Scissors'
    computer = random.choice(choices)

    if computer == 'paper':
        result = 'scissors cut paper, you win'
    elif computer == 'Spock':
        result = 'Spock smashes scissors, you loose'
    elif computer == 'lizard':
        result = 'scissors decapitates lizard, you win'
    elif computer == 'rock':
        result = 'rock crushes scissors, you loose'
    elif computer == 'scissors':
        result = 'tie'
    else:
        result = '????'

    return render(request, 'scissors.html', {'choice': choice, 'result': result, 'computer': computer})


def lizard(request):
    choice = 'Lizard'
    computer = random.choice(choices)

    if computer == 'rock':
        result = 'rock crushes lizard, you loose'
    elif computer == 'Spock':
        result = 'lizard poisons Spock, you win'
    elif computer == 'scissors':
        result = 'scissors decapitates lizard, you loose'
    elif computer == 'paper':
        result = 'lizard eats paper, you win'
    elif computer == 'lizard':
        result = 'tie'
    else:
        result = '????'

    return render(request, 'lizard.html', {'choice': choice, 'result': result, 'computer': computer})


def spock(request):
    choice = 'Spock'
    computer = random.choice(choices)

    if computer == 'lizard':
        result = 'lizard poisons Spock, you loose'
    elif computer == 'scissors':
        result = 'Spock smashes scissors, you win'
    elif computer == 'paper':
        result = 'paper disproves Spock, you loose'
    elif computer == 'rock':
        result = 'Spock vaporizes rock, you win'
    elif computer == 'Spock':
        result = 'tie'
    else:
        result = '????'

    return render(request, 'spock.html', {'choice': choice, 'result': result, 'computer': computer})
